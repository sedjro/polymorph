#!/usr/bin/env python

"""

get information from phosphoprotein where SNP is not within 7 AA of phosphorylate residue

"""

import csv, os, sys

def get_args(cline = sys.argv[1:]):

    '''
    get argument in command line
    '''
    
    import argparse
    parser = argparse.ArgumentParser(description = '''get information from phosphoprotein where SNP is not within 7 AA of phosphorylate residue''')
    parser.add_argument('input_file', help = 'Pilot individu file')
    parser.add_argument('-p', '--protein', help = 'phosphoprotein file')
    args = parser.parse_args(cline)
    return args

def prot_info(input_file, prot_file):

    ind_data = []

    with open(input_file, 'rU') as csvfile:
        testreader = csv.reader(csvfile, delimiter = ',')
        for row in testreader:
            ind_data.append('\t'.join(row))

 	dom_file = '/'.join([os.path.dirname(input_file), os.path.basename(input_file).replace('.csv', '.txt')])
 	dom_info = open(dom_file, 'w')

    prot_data = open(prot_file, 'r')
    data_list = prot_data.readlines()
    prot_data.close()

    for ind_line in ind_data:

        prot_id = ind_line.split('\t')[0]
        pho_dom = ind_line.split('\t')[14]
        dom_class = ind_line.split('\t')[15]

        test_line = False
        print prot_id

        for prot_line in data_list:

            prot_acc = prot_line.split('\t')[1]
            prot_dom = prot_line.split('\t')[9]

            if prot_id == prot_acc:

                test_line = True

                if dom_class == '-' or dom_class == 'phosphoprotein':
                    dom_info.write('\t'.join(['\t'.join(ind_line.split('\t')[:15]), 'phospho binding domain', prot_dom, '\t'.join(ind_line.split('\t')[16:])]) + '\n')
                    break
                elif dom_class == 'phosphoprotein-kinase':
                    dom_info.write('\t'.join(['\t'.join(ind_line.split('\t')[:15]), 'kinase/phospho binding domain', prot_dom, '\t'.join(ind_line.split('\t')[16:])]) + '\n')
                    break
                elif dom_class == 'phosphoprotein-phosphatase':
                    dom_info.write('\t'.join(['\t'.join(ind_line.split('\t')[:15]), 'phosphatase/phospho binding domain', prot_dom, '\t'.join(ind_line.split('\t')[16:])]) + '\n')
                    break
                elif dom_class == 'phosphoprotein-kinase-phosphatase':
                    dom_info.write('\t'.join(['\t'.join(ind_line.split('\t')[:15]), 'kinase/phosphatase/phospho binding domain', prot_dom, '\t'.join(ind_line.split('\t')[16:])]) + '\n')
                    break
                else:
                    dom_info.write('\t'.join(['\t'.join(ind_line.split('\t')[:15]), 'phospho binding domain', prot_dom, '\t'.join(ind_line.split('\t')[16:])])  + '\n')
                    break

        if not test_line:

            if  dom_class == 'kinase-phosphatase':
                dom_info.write('\t'.join(['\t'.join(ind_line.split('\t')[:15]), 'kinase/phosphatase', '-', '\t'.join(ind_line.split('\t')[16:])])  + '\n')
            else:
                dom_info.write('\t'.join(['\t'.join(ind_line.split('\t')[:16]), '-', '\t'.join(ind_line.split('\t')[16:])])  + '\n')

        test_line = False
        
#####################################################################################################

if __name__ == '__main__':
    args = get_args()
    prot_info(args.input_file, args.protein)