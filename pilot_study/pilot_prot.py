#!/usr/bin/env python

import sys

def get_args(cline = sys.argv[1:]):

    '''
    get argument in command line
    '''
    
    import argparse
    parser = argparse.ArgumentParser(description = '''get information from phosphoprotein where SNP is not within 7 AA of phosphorylate residue''')
    parser.add_argument('input_file', help = 'Pilot individu temp file with phosphoprotein information')
    parser.add_argument('-k', '--kinase', help = 'protein kinase file')
    parser.add_argument('-p', '--phosphatase', help = 'protein phosphatase file')
    args = parser.parse_args(cline)
    return args

def additional_info(pilot_temp, kinase_file, phosphatase_file):

	pilot = open(pilot_temp, 'r')
	kin = open(kinase_file, 'r')
	phos = open(phosphatase_file, 'r')
	pil = open(pilot_temp.replace('_temp', '_prot'), 'w')

	pilot_data = pilot.readlines()
	kinase_data = kin.readlines()
	phos_data = phos.readlines()

	phos.close()
	pilot.close()

	kinase_list = []

	for pilot_line in pilot_data:

		test = False
		dom = ""
		bind = ""

		for kinase_line in kinase_data:

			if pilot_line.split('\t')[1] == kinase_line.split('\t')[2]:

				test = True

				if pilot_line.split('\t')[15] != '-' :
					dom = pilot_line.split('\t')[15] + '-kinase'
				else:
					dom = 'kinase'
					bind = 'yes'
				break

		for site_line in site_data:

			if pilot_line.split('\t')[1] == site_line.split('\t')[2]:

				test = True

				if pilot_line.split('\t')[15] != '-' :
					if dom:
						dom = dom + '-phosphatase'
					else:
						dom =  pilot_line.split('\t')[15] + '-phosphatase'	
				else:
					if dom:
						dom = dom + '-phosphatase'
					else:
						dom = 'phosphatase'
						bind = 'yes'
				break

		if test:
			pil.write('\t'.join(['\t'.join(pilot_line.split('\t')[:14]), bind, dom, '\t'.join(pilot_line.split('\t')[16:])]))
		else:
			pil.write(pilot_line)

####################################################################################################

if __name__ == '__main__':
    args = get_args()
    phospho_data(args.input_file, args.kinase, args.phosphatase)






