#!/usr/bin/env python

import re, sys, os

def get_args(cline = sys.argv[1:]):

    '''
    get argument in command line
    '''
    
    import argparse
    parser = argparse.ArgumentParser(description = '''get information from phosphoprotein where SNP is not within 7 AA of phosphorylate residue''')
    parser.add_argument('input_file', help = 'Pilot individu file')
    parser.add_argument('-p', '--protein', help = 'phosphoprotein file')
    args = parser.parse_args(cline)
    return args


def phospho_data(pilot_file, phospho_file):


	pilot = open(pilot_file, 'r')
	phospho = open(phospho_file, 'r')
	pil = open('_'.join([input_file, 'temp']), 'w')

	pilot_data = pilot.readlines()
	site_data = phospho.readlines()

	pilot.close()
	phospho.close()

	for pilot_line in pilot_data:

		test = False

		for site_line in site_data:

			if pilot_line.split('\t')[1] == site_line.split('\t')[2]:

				test = True
			
				if len(pilot_line.split('\t')[12]) > 1:
					if re.search('[STY]', pilot_line.split('\t')[12]) or re.search('[STY]', pilot_line.split('\t')[13]): 
						pil.write('\t'.join(['\t'.join(pilot_line.split('\t')[:12]), '{}', pilot_line.split('\t')[13].split('\n')[0], 'yes', 'phosphoprotein', 'yes', 'yes']) + '\n')
					else:
						pil.write('\t'.join(['\t'.join(pilot_line.split('\t')[:12]), '{}', pilot_line.split('\t')[13].split('\n')[0], 'yes', 'phosphoprotein', 'yes', 'no']) + '\n')

				elif len(pilot_line.split('\t')[13].split('\n')[0]) > 1:
					if re.search('[STY]', pilot_line.split('\t')[12]) or re.search('[STY]', pilot_line.split('\t')[13]): 
						pil.write('\t'.join(['\t'.join(pilot_line.split('\t')[:13]), '[]', 'yes', 'phosphoprotein', 'yes', 'yes']) + '\n')
					else:
						pil.write('\t'.join(['\t'.join(pilot_line.split('\t')[:13]), '[]', 'yes', 'phosphoprotein', 'yes', 'no']) + '\n')

				else:
					if re.search('[STY]', pilot_line.split('\t')[12]) or re.search('[STY]', pilot_line.split('\t')[13]): 
						pil.write('\t'.join([pilot_line.split('\n')[0], 'yes', 'phosphoprotein', 'yes', 'yes']) + '\n')
					else:
						pil.write('\t'.join([pilot_line.split('\n')[0], 'yes', 'phosphoprotein', 'yes', 'no']) + '\n')
				break

		if not test:

			if len(pilot_line.split('\t')[12]) > 1:
				if re.search('[STY]', pilot_line.split('\t')[12]) or re.search('[STY]', pilot_line.split('\t')[13]): 
					pil.write('\t'.join(['\t'.join(pilot_line.split('\t')[:12]), '{}', pilot_line.split('\t')[13].split('\n')[0], 'no', '-', 'no', 'yes']) + '\n')
				else:
					pil.write('\t'.join(['\t'.join(pilot_line.split('\t')[:12]), '{}', pilot_line.split('\t')[13].split('\n')[0], 'no', '-', 'no', 'no']) + '\n')

			elif len(pilot_line.split('\t')[13].split('\n')[0]) > 1:
				if re.search('[STY]', pilot_line.split('\t')[12]) or re.search('[STY]', pilot_line.split('\t')[13]): 
					pil.write('\t'.join(['\t'.join(pilot_line.split('\t')[:13]), '[]', 'no', '-', 'no', 'yes']) + '\n')
				else:
					pil.write('\t'.join(['\t'.join(pilot_line.split('\t')[:13]), '[]', 'no', '-', 'no', 'no']) + '\n')

			else:
				if re.search('[STY]', pilot_line.split('\t')[12]) or re.search('[STY]', pilot_line.split('\t')[13]): 
					pil.write('\t'.join([pilot_line.split('\n')[0], 'no', '-', 'no', 'yes']) + '\n')
				else:
					pil.write('\t'.join([pilot_line.split('\n')[0], 'no', '-', 'no', 'no']) + '\n')

#####################################################################################################

if __name__ == '__main__':
    args = get_args()
    phospho_data(args.input_file, args.protein)


		
	