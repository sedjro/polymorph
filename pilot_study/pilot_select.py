#! /usr/bin/env python

import sys, re


def get_args(cline = sys.argv[1:]):

    '''
    get argument in command line
    '''
    
    import argparse
    parser = argparse.ArgumentParser(description = '''Get information for 3 pilot individuals ''')
    parser.add_argument('input_file', help = 'list of SNP in protein involved in phosphorylation network')
    args = parser.parse_args(cline)
    return args


def selection(input_file):
        
    all_ind = open(input_file, 'r')
    ind_1 = open("/home/yfourne/chicago_test/pilot_study/freq_5/pilot_18486", 'a')
    ind_2 = open("/home/yfourne/chicago_test/pilot_study/freq_5/pilot_18862", 'a')
    ind_3 = open("/home/yfourne/chicago_test/pilot_study/freq_5/pilot_19160", 'a')

    while True:    
        line = all_ind.readline()
        if not line:
            break

        info = line.split('\t')

        try:
            freq = float(info[6])
        except:
            freq = '-'
        
        try:
            pos = re.split(r'(\d+)', info[12])[1]
        except:
            pos = '-'
        
        try:
        	prt_id = info[18+121]
        except:
        	prt_id = '-'

        if freq != '-' and freq >= 0.05:

            if info[18+1] != info[18+108]:
                if info[18+1] != info[18+48]:

                    if info[18+1] == '1|1':
                        ind_1.write('\t'.join([info[2], 'yes', 'no', info[3], info[4], pos, '18862-19160', '-', prt_id]) + '\n')
                    elif info[18+1] == '1|0' or info[18+1] == '0|1':
                        ind_1.write('\t'.join([info[2], 'no', 'yes', info[3], info[4], pos, '18862-19160', '-', prt_id]) + '\n')
                    
                    if info[18+108] != info[18+48]:

                        if info[18+108] == '1|1':
                            ind_2.write('\t'.join([info[2], 'yes', 'no', info[3], info[4], pos, '18486-19160', '-', prt_id]) + '\n')
                        elif info[18+108] == '1|0' or info[18+108] == '0|1':
                            ind_2.write('\t'.join([info[2], 'no', 'yes', info[3], info[4], pos, '18486-19160', '-', prt_id]) + '\n')

                        if info[18+48] == '1|1':
                            ind_3.write('\t'.join([info[2], 'yes', 'no', info[3], info[4], pos, '18486-18862', '-', prt_id]) + '\n')
                        elif info[18+48] == '1|0' or info[18+48] == '0|1':
                            ind_3.write('\t'.join([info[2], 'no', 'yes', info[3], info[4], pos, '18486-18862', '-', prt_id]) + '\n')

                    else:

                        if info[18+108] == '1|1':
                            ind_2.write('\t'.join([info[2], 'yes', 'no', info[3], info[4], pos, '18486', '19160', prt_id]) + '\n')
                        elif info[18+108] == '1|0' or info[18+108] == '0|1':
                            ind_2.write('\t'.join([info[2], 'no', 'yes', info[3], info[4], pos, '18486', '19160', prt_id]) + '\n')

                        if info[18+48] == '1|1':
                            ind_3.write('\t'.join([info[2], 'yes', 'no', info[3], info[4], pos, '18486', '18862', prt_id]) + '\n')
                        elif info[18+48] == '1|0' or info[18+48] == '0|1':
                            ind_3.write('\t'.join([info[2], 'no', 'yes', info[3], info[4], pos, '18486', '18862', prt_id]) + '\n')

                else:

                    if info[18+1] == '1|1':
                        ind_1.write('\t'.join([info[2], 'yes', 'no', info[3], info[4], pos, '18862', '18486', prt_id]) + '\n')
                    elif info[18+1] == '1|0' or info[18+1] == '0|1':
                        ind_1.write('\t'.join([info[2], 'no', 'yes', info[3], info[4], pos, '18862', '18486', prt_id]) + '\n')

                    if info[18+48] == '1|1':
                        ind_3.write('\t'.join([info[2], 'yes', 'no', info[3], info[4], pos, '18862', '19160', prt_id]) + '\n')
                    elif info[18+48] == '1|0' or info[18+48] == '0|1':
                        ind_3.write('\t'.join([info[2], 'no', 'yes', info[3], info[4], pos, '18862', '19160', prt_id]) + '\n')

            else:

                if info[18+1] == '1|1':
                    ind_1.write('\t'.join([info[2], 'yes', 'no', info[3], info[4], pos, '19160', '18862', prt_id]) + '\n')
                elif info[18+1] == '1|0' or info[18+1] == '0|1':
                    ind_1.write('\t'.join([info[2], 'no', 'yes', info[3], info[4], pos, '19160', '18862', prt_id]) + '\n')

                if info[18+108] == '1|1':
                    ind_2.write('\t'.join([info[2], 'yes', 'no', info[3], info[4], pos, '19160', '18486', prt_id]) + '\n')
                elif info[18+108] == '1|0' or info[18+108] == '0|1':
                    ind_2.write('\t'.join([info[2], 'no', 'yes', info[3], info[4], pos, '19160', '18486', prt_id]) + '\n')

#####################################################################################################

if __name__ == '__main__':
    args = get_args()
    selection(args.input_file)