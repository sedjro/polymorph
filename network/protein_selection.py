#!/usr/bin/env python

'''
Selection of pretien in Lymphoblastoid Cells Lines 
'''

import sys, os, re
from Bio import SeqIO

def get_args(cline = sys.argv[1:]):
    '''
    get input file command line and output file command line
    
    '''
    import argparse
    parser = argparse.ArgumentParser(description = '''Convert table data file to fasta file''')
    
    parser.add_argument('input_file', help = 'fasta file with all protein')
    parser.add_argument('-f', '--file_list', help = 'file of list of measured protein')
    parser.add_argument('-n', '--no_fileProt', help = 'file of lcl protein not in pospho network')
    parser.add_argument('-o', '--output_file', help = 'output fasta file [default: stdout]',
                        metavar = "fname.fa")
    args = parser.parse_args(cline)

    return args
    
def studied_protein(Inp_file, file_list, no_lcl, Outp_file):

    lcl_list = open(file_list, 'r')
    NoLcl = open(no_lcl, 'w')
    lcl_prot = open(Outp_file, 'w')

    while True:
        
        Lline = lcl_list.readline()
        if not Lline:
            break
                
        ref_name = Lline.split('\n')[0]
        no_prot = True

        for seq_record in SeqIO.parse(Inp_file, "fasta"):
            if re.search(ref_name, seq_record.id):
                no_prot = False
                lcl_prot.write('>' + seq_record.id + '\n')
                lcl_prot.write(str(seq_record.seq) + '\n')
                
        if no_prot:
            NoLcl.write(ref_name + '\n')
            
				
								
#####################################################################################################        
        
if __name__ == '__main__':
    args = get_args()
    if args.output_file:
        studied_protein(args.input_file, args.file_list, args.no_fileProt, args.output_file)
    else:
        studied_protein(args.input_file, args.file_list, args.no_fileProt)
	