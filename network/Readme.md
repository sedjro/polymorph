Theorical Modification
============================================



*************************************************

With the result file from link processing.
we can change amino acid according to genotype

> N.B.:
for heterozygote genotype with chose to applied the amino acid corresponding to the minor allele frequencies

*************************************************

In the dataset from phosphorylation domain every protein sequence is available Pfam database
but fron phosphosite or phospho.ELM, it is necessary to get sequences. Normally every protein is in SMART or Pfam database but if you need you can used biomaRt.

**Essential**
Get the gene ensembl id accroding to your protein or protein id
 
If you used biomaRt to get sequences, Used this script to change the tab file format to fasta file format

+ tab_to_fasta.py 

*************************************************


## First Step

You have to constituate the list of SNPs usable.
In other, selection of one transcript per SNPs and filter out the synonymous SNP.

With link file used:
  
+ useful_snp.R
+ split_snp.R

> N.B.:
In this scrip, thinking to change the path folder

## Seconde Step

If you work with specific cell, you have to selected protein that you can detect in your cells
here, we work with lymphoblastoid cell line. 

You need to a list of cell protein
a single column with ensemble id or protein id (id in protein fasta file) 

+ protein_selection.py


## Fird Step

change the amino acid in each protein for Non_Synonymous SNPs for each individual
you need the both file SNPs usable and protein fasta file.

+ individual_phenotype.py


## Supplementary Step (Switches.ELM)

Get SNP involved in Switches.ELM 

Download the switches.ELM-v1.txt from http://switches.elm.eu.org/index.php?page=help

with this file and usable SNPs, run this script:

+ cross_switchesELM.py



