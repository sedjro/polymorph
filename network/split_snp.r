
rm(list=ls())
dir_path <- "/Users/Sedjro/clusterhome/result/network/reference/reference_species_variations/temp/"
file_list <- dir(dir_path)


NonSynonym <- c("CODON_CHANGE_PLUS_CODON_DELETION", 
                "CODON_CHANGE_PLUS_CODON_INSERTION", 
                "CODON_DELETION", "CODON_INSERTION", "EXON", "EXON_DELETED", 
                "FRAME_SHIFT", "NON_SYNONYMOUS_CODING", "NON_SYNONYMOUS_START", 
                "SPLICE_SITE_REGION", "START_GAINED", "START_LOST", "STOP_GAINED", 
                "STOP_LOST")

Synonym <- c("SYNONYMOUS_CODING", "SYNONYMOUS_STOP", "UTR_3_PRIME", "UTR_5_PRIME")


for (file in file_list){
  NS_tab <- NULL
  S_tab <- NULL
  data <- read.delim(paste(dir_path, file, sep = '/'), header = FALSE, sep = '\t')
  
  for (eff in unique(data$V13)){
    sym <- FALSE
    for (SL in Synonym){
      if (eff == SL)
        sym <- TRUE
    }
    if (sym){
      S_tab <- rbind(S_tab,  data[data$V13 == eff,])
    }else{
      NS_tab <- rbind(NS_tab, data[data$V13 == eff,])
    }    
  }
  NS_out_dir_path <- "/Users/Sedjro/clusterhome/result/network/reference/reference_species_variations/NStemp/"
  S_out_dir_path <- "/Users/Sedjro/clusterhome/result/network/reference/reference_species_variations/Stemp/"

  write.table(NS_tab, file= paste(NS_out_dir_path, file, sep = '/'), sep = '\t', row.names = FALSE, col.names = FALSE, quote = FALSE)
  write.table(S_tab, file= paste(S_out_dir_path, file, sep = '/'), sep = '\t', row.names = FALSE, col.names = FALSE, quote = FALSE)
}  
  