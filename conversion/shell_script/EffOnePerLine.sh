#!/bin/bash


<<instruction

Run 'SnpSift.jar extractFields' create a vcf with one effect by line from vcf annotate by SnfEff
It's need 2 argument : Input folder with to process and output folder   

instruction

INPUT_DIR=$1
RESULT_DIR=$2

for filename in `ls $INPUT_DIR/*.snpeff.vcf`
do
    echo "cat $filename | /mnt/lustre/home/y.fourne/tools/snpEff/scripts/vcfEffOnePerLine.pl > \
$RESULT_DIR/`basename ${filename%snpeff.vcf}`EffPerLine.vcf" | \
	qsub -l h_vmem=16g -N `basename ${filename%snpeff.vcf}`EffPerLine.vcf -o $RESULT_DIR -j y -cwd -V
done
