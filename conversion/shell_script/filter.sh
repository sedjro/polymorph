#!/bin/bash


<<instruction 

Run SnpSift.jar filter with a list of specific effect
It's need 2 argument : Input folder with to process and output folder for vcf output file  

instruction

INPUT_DIR=$1
RESULT_DIR=$2

Effect_list="CODON_CHANGE_PLUS_CODON_DELETION CODON_CHANGE_PLUS_CODON_INSERTION \
CODON_DELETION CODON_INSERTION EXON EXON_DELETED FRAME_SHIFT NON_SYNONYMOUS_CODING \
NON_SYNONYMOUS_START SPLICE_SITE_REGION START_GAINED START_LOST STOP_GAINED \
STOP_LOST SYNONYMOUS_CODING SYNONYMOUS_STOP UTR_3_PRIME UTR_5_PRIME"

for filename in  `ls $INPUT_DIR/*.EffPerLine.vcf`
do
    mkdir "$INPUT_DIR/`basename ${filename%.EffPerLine.vcf}`_filter"
    Effect_dir="$INPUT_DIR/`basename ${filename%.EffPerLine.vcf}`_filter"
    for effect in $Effect_list
    do
	echo "cat $filename | java -jar /home/y.fourne/tools/snpEff/SnpSift.jar filter \"(EFF[*].EFFECT = '$effect') & (EFF[*].CODING = 'CODING')\" \
>  $Effect_dir/`basename ${filename%EffPerLine.vcf}`$effect.vcf" | \
	    qsub -l h_vmem=16g -N `basename ${filename%EffPerLine.vcf}`$effect.vcf -o $Effect_dir -j y -cwd -V

    done
    
    echo "java -jar /home/y.fourne/tools/snpEff/SnpSift.jar split -j $Effect_dir/*  > $INPUT_DIR/`basename ${filename%EffPerLine.vcf}`filter.vcf" | \
	qsub -l h_vmem=16g -N `basename $filename` -o $RESULT_DIR -j y -cwd -V

done
