#!/bin/bash


<<instruction 

Run convert_impute2_to_vcf.py python program on all _haps.gz file in a folder
It's need 2 argument : Input folder with to process and output folder for vcf output file  

instruction

IMPUTE_DIR=$1
RESULT_DIR=$2

for filename in `ls $IMPUTE_DIR/*_haps.gz`
do
  echo "/mnt/lustre/home/y.fourne/polymorph/convert_impute2_to_vcf.py $filename > $RESULT_DIR/`basename ${filename%gz}`vcf" | \
    qsub -l h_vmem=4g -N `basename $filename` -o $RESULT_DIR -j y -cwd -V
 
done
