# Yannick Fourne
# Link protein ID with gene ID


rm(list=ls())

library("biomaRt")
source("http://bioconductor.org/biocLite.R")
ensembl = useMart("ensembl",dataset="hsapiens_gene_ensembl")


chr_list <- c(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,'X','Y')
dir_path <- "/Users/Sedjro/Desktop/internship/linkage_phospho/R_link/get_gen_info_script"
file_list <- dir(dir_path)

for (file in file_list){
  data <- read.delim(paste(dir_path, file, sep = '/'), header = FALSE)
  
  #remove portein code extention "." or "-" 
  check2 <- NULL
  for (check1 in data[,1]){
    check2 <- c(check2, strsplit(check1, ".", fixed = TRUE)[[1]][1])
  }
  code_list <- NULL
  for (check in check2){
    code_list <- c(code_list, strsplit(check, "-", fixed = TRUE)[[1]][1])
  }
  
  #get information from uniprot-swissprot accession number
  first_out <- getBM(attributes=c('peptide', 'chromosome_name', 'start_position', 'end_position',
                                  'ensembl_gene_id', 'hgnc_symbol', 'uniprot_swissprot_accession'), 
                     filters = 'uniprot_swissprot_accession', 
                     values = code_list, mart = ensembl)
  
  if (length(code_list) != nrow(first_out)){ 
    new_code_list <- setdiff(code_list, first_out$uniprot_swissprot_accession) #remove gene found
    
    #get information from uniprot-swissprot accession number
    second_out <- getBM(attributes=c('peptide', 'chromosome_name', 'start_position', 'end_position',
                                     'ensembl_gene_id', 'hgnc_symbol', 'uniprot_sptrembl'), 
                        filters = 'uniprot_sptrembl', 
                        values = new_code_list, mart = ensembl)    
  }
  colnames(first_out)[7] <- "prot_id"
  colnames(second_out)[7] <- "prot_id"
  both_out <- rbind(first_out, second_out)
  both_out$peptide <- NULL 
  out <- both_out[both_out$chromosome_name %in% chr_list, ]
  out_dir_path <- "/Users/Sedjro/Desktop/internship/linkage_phospho/output_Rlink"
  write.table(out, file= paste(out_dir_path, file, sep = '/'), sep = '\t', row.names = FALSE, quote = FALSE)

}

