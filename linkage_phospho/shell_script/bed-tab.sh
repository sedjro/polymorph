#!/bin/bash


<<instruction 

Run conversion_bed_txt.py python program on all intersect bed file in a folder
It's need 2 argument : Input folder with to process and output folder for txt output file  

instruction

IMPUTE_DIR=$1
RESULT_DIR=$2

for filename in `ls $IMPUTE_DIR/*`
do
  echo "/mnt/lustre/home/y.fourne/polymorph/linkage_phospho/conversion_bed_txt.py $filename > $RESULT_DIR/`basename $filename`.txt" | \
    qsub -l h_vmem=4g -N `basename $filename` -o $RESULT_DIR -j y -cwd -V
 
done
