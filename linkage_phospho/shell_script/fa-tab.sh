#!/bin/bash


<<instruction 

Run fasta_file_info.py python program on all file in a folder
It's need 2 argument : Input folder with to process and output folder for output file  

instruction

IMPUTE_DIR=$1
RESULT_DIR=$2

for filename in `ls $IMPUTE_DIR/*.fa`
do
    /mnt/lustre/home/y.fourne/polymorph/linkage_phospho/script/fasta_file_info.py $filename -s smart > $RESULT_DIR/`basename ${filename%.fa}` 
 
done
