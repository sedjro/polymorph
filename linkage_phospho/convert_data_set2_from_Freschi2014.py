#!/usr/bin/env python

'''
Conversion specific dataset from supplementary data set 2 from Freschi et al. 2014 to a tab-separated file. 
The columns should be ensembl_id, species, position, amino_acid, disorded, and phosphorlyated.

'''

import sys, os

def get_args(cline = sys.argv[1:]):
    '''
    '''
    import argparse
    parser = argparse.ArgumentParser(description = '''conversion specific dataset.''')
    parser.add_argument('input_file', help = 'TXT file')
    parser.add_argument('-o', '--output', help = 'tab file file [default: stdout]',
                        metavar = "fname.txt")
    args = parser.parse_args(cline)
    return args



def get_info(input_file, tab=sys.stdout):

	file = open(input_file, 'r')
	tab.write("ensembl_id\tspecies\tposition\tamino_acid\tdisorded\tphosphorlyated\tpublication\n")
	
	while True:
		line = file.readline()
		if not line:
			break
	
		if line[0] == 'P':
			code = line.split(':')[1].split(',')
			continue
		elif line[0] != 'P' and line[0] != 'h' and line[0] != '\n':
			data = line.split(',') 
			data[2] = data[2].replace('.', '0')
			data[2] = data[2].replace('*', '1')
			data[6] = data[6].replace('.', '0')
			data[6] = data[6].replace('*', '1')
		else:
			continue
		
		if data[10]: 
			paper_1 = data[10]
		else:
			paper_1 = 'NA'
		if data[11].split() is None: 
			paper_2 = data[11].split()[0]
		else:
			paper_2 = 'NA'
		
		
		H_info = str.join('\t', [code[0][1:], "Homo_sapiens", str.join('\t', data[0:4]), paper_1])
		M_info = str.join('\t', [code[1], "Mus_musculus", str.join('\t', data[4:8]), paper_2])
		C_info = str.join('\t', [code[2], "Canis_familiaris", "NA", data[8], "NA\tNA\tNA"])
		P_info = str.join('\t', [code[3].split()[0], "Didelphis_marsupialis", "NA", data[9], "NA\tNA\tNA"])
			
		tab.write(H_info + '\n' + M_info + '\n' + C_info + '\n' + P_info + '\n')
	
	
#####################################################################################################

if __name__ == '__main__':
    args = get_args()
    if args.output:
        tab = open(args.tab, 'w')
        get_info(args.input_file, tab)
    else:
        get_info(args.input_file)






