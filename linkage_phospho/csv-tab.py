
cs#!/usr/bin/env python

input_file = open("/mnt/lustre/home/y.fourne/polymorph/linkage_phospho/Hupho/PhosphoClass_2014-07-11_21-49.csv", 'r')
output_file = open("/mnt/lustre/home/y.fourne/polymorph/linkage_phospho/R_treatment/PhosphoClass", 'w')

while True:
	line = input_file.readline()
	if not line:
		break
	newLine = line.replace(';', '\t')
	output_file.write(newLine)
	
input_file.close()
output_file.close()