#!/usr/bin/env python

'''

Get variation involved in phosphorylation protein network
Linkage between variation file (SNPs or species difference) and gene coding protein position 

'''

from pybedtools import BedTool
import sys, os


def get_args(cline = sys.argv[1:]):
    '''
    get input file command line and output file command line
    
    '''
    import argparse
    parser = argparse.ArgumentParser(description = '''Linkage between variation file\
     and gene coding protein position ''')
    
    parser.add_argument('variant_file', help = 'Variation file in VCF format')
    parser.add_argument('-r', '--R_file', help = 'Gene coding protein R file')    
    args = parser.parse_args(cline)

    return args
    
def get_link (variant_file, R_file):
    
    output_file = os.path.basename(R_file)
    bed_folder =  os.path.dirname(os.path.dirname(R_file)) + '/output_BedFile'
    if not os.path.exists(bed_folder):
        os.makedirs(bed_folder)
        
    bed_file =  bed_folder + '/' + output_file +  '.bed'
    Rfl = open(R_file, 'r')
    bd = open(bed_file, 'w') 
    t_line = Rfl.readline()
    element_nb = len(t_line.split())

    while True:
        t_line = Rfl.readline()
        if not t_line:
            break
        info = t_line.split()
        info[0] = 'chr' + info[0]
        if len(info) != element_nb:
            while len(info) < element_nb:
                info.insert(4, 'NA')
        nline = str.join('\t', info)
        bd.write(nline + '\n')

    bd.close()

    lkg_folder = os.path.dirname(variant_file) + '/link/lkg'
    lk_folder = os.path.dirname(variant_file) + '/link/lk'
    if not os.path.exists(lkg_folder):
        os.makedirs(lkg_folder)
    if not os.path.exists(lk_folder):
        os.makedirs(lk_folder)
    
    lkg_file = lkg_folder + '/lkg_' + output_file
    lk_file = lk_folder + '/lk_' + output_file  

    snp = BedTool(variant_file)
    gene = BedTool(bed_file)           
    link = snp.intersect(gene, wb=True)    
    link.moveto(lkg_file)   
        
    link_gen_file = open(lkg_file, 'r')
    link_lg_file = open(lk_file, 'w')
    
    while True:
        gnt_line = link_gen_file.readline()
        if not gnt_line:
            break
        nw_line = str.join('\t', [str.join('\t', gnt_line.split()[:8]), str.join('\t', gnt_line.split()[-5:])])         
        link_lg_file.write(nw_line + '\n')
        
    link_gen_file.close()
    link_lg_file.close()    
 
#####################################################################################################        
        
if __name__ == '__main__':
    args = get_args()
    get_link(args.variant_file, args.R_file)
	
    